import { AuthService } from './../_shared/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { User } from '../_shared/user';


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  profileID: string;
  currentUser: User;

  constructor(
    public authService: AuthService,
    private actRoute: ActivatedRoute
  ) {

    this.profileID = this.actRoute.snapshot.params.id; 

  }

  ngOnInit(): void {
    this.authService.getUserProfile()
      .subscribe(res => {
        this.currentUser = res[0];
        console.log("The current user is " + JSON.stringify(this.currentUser))
      });
  }

}

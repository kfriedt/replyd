
import { Component, Output, EventEmitter, OnDestroy, Input, OnInit } from '@angular/core';
import { AuthService } from './_shared/auth.service';
import { SearchBarService } from './search-bar/search-bar.service';
import { Subject } from 'rxjs';
import { School } from './schools/schools';
import { Program } from './programs/programs';
import { Videos } from './videos/videos';
import { Courses } from './courses/courses';
import { debounceTime } from 'rxjs/operators';
import { SearchService } from './search.service';
import { User } from './_shared/user';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnDestroy, OnInit {
  title = 'Curicula-Frontend';
  //faCoffee = faCoffee; //font awesome coffee icon

  schoolArr: School [] = [];
  programArr: Program [] = [];
  videoArr: Videos [] = [];
  coursesArr: Courses[] = [];


  currentUser: User [] = [];
  destroy$: Subject<boolean> = new Subject<boolean>();


  public appSearchSub: Subject<string> = new Subject();
  @Output() setValue: EventEmitter<string> = new EventEmitter();
  @Input() readonly placeholder: string = '';
  // tslint:disable-next-line: max-line-length
  constructor(private searchService: SearchBarService, public authService: AuthService, private searchBarService: SearchService, public router: Router, private toastr: ToastrService) {
    this.setSearchSubscription();

  }
  // constructor(public authService: AuthService) { }

  ngOnInit(): void {
    this.populateContentArray();
  }


  private setSearchSubscription() {
    this.appSearchSub.pipe(
      debounceTime(500)
    ).subscribe((searchValue: string) => {
      // Filter Function
      this.setValue.emit(searchValue);  // WHAT DOES THIS DO
    });
  }

  public updateSearch(searchTextValue: string) {
    this.appSearchSub.next( searchTextValue );
    this.searchBarService.setSearchData(searchTextValue);
    console.log(searchTextValue);

  }

  navigateToUserProfile(){
    console.log("nav to user profile called")
    this.router.navigate(['user-profile/' + localStorage.getItem('user_id')]);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
    this.appSearchSub.unsubscribe();
  }

  // onClickSearchButton(){
  //   console.log("onClick event triggered");
  //   this.email = (<HTMLInputElement>document.getElementById("searchBar")).value;
  //   console.log(this.email)
  // }
  onInput() {
    console.log('on input triggered');
  }

  logout() {
    this.authService.doLogout();
  }

  populateContentArray()
  {
    this.searchService.getSchools().subscribe(dataRecv => {
      this.schoolArr = dataRecv;
    });

    this.searchService.getPrograms().subscribe(dataRecv => {
      this.programArr = dataRecv;
    });

    this.searchService.getVideos().subscribe(dataRecv => {
      this.videoArr = dataRecv;
    });

    this.searchService.getCourses().subscribe(dataRecv => {
      this.coursesArr = dataRecv;
    });

  }
}



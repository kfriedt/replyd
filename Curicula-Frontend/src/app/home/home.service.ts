import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Home } from './home';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {


  testUrl = 'https://reqres.in/api/users?page=2';

  constructor(private http: HttpClient) { }

  httpOptions = { headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };


  getPeople(): Observable<any>{
    console.log('RIGHT BELOW HERE');

    return this.http.get<Home[]>(this.testUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

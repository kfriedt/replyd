import { Subject } from 'rxjs';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { Home } from './home';
import { HomeService } from './home.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']

})
export class HomeComponent implements OnInit, OnDestroy {

  peopleArray: Home[] = []; // Array has a type that is from the interface in the homes.ts file
  // the homes.ts file is imported into this module.

  // this object is used to unsubscribe from the observable.
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private homeService: HomeService) { }
  // private homeService: HomeService
  ngOnInit() {
    // this.populatePeopleArray();
  }


  // This method is called in the Init of the page, it subscribes to the observable object
  // that is returned by the HomeService method getPeople()
  populatePeopleArray() {
    this.homeService.getPeople().subscribe(dataRecv => {
      console.log(dataRecv.data); // Had dataRecv['data']; before


       // tslint:disable-next-line: max-line-length
      this.peopleArray = dataRecv.data; // Had dataRecv['data']; before ;
      // THIS IS WORKING TO PASTE THE OBJECTS INTO THE ARRAY - you need the ['data']
      // because the array was contained within 'data' padding in the JSON
      console.log('Before first Element');
      // Testing other ways of getting the data.
      let firstElementInArray = this.peopleArray[0]; // THIS IS WORKING
      console.log(firstElementInArray);  // THIS IS WORKING
      console.log('Before .id');
      console.log(firstElementInArray.id); // THIS IS WORKING
   });
  }

  ngOnDestroy() {
    // this.destroy$.next(true);
    // // Unsubscribe from the subject
    // this.destroy$.unsubscribe();
  }


}


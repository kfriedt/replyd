import { AuthService } from './../_shared/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { UserJWT } from '../_shared/user-jwt';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  flag: number
  signinForm: FormGroup;
  submitted = false;
  email2: string;
  password2: string;
  endpoint: string = 'https://curicular-api.herokuapp.com/user';
  showIncorrectUsernamePassword: number = 0;
  showErrorOccured: number = 0;

   constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    private toastr: ToastrService,
    private http: HttpClient
  ) {
    this.signinForm = this.fb.group({
      email: ['' , Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }
  get form() { return this.signinForm.controls; }

  loginUser() {
    this.submitted = true;
    this.email2 = this.form.email.value
    this.password2 = this.form.password.value

    var email = this.email2
    var password = this.password2

    if (this.email2 != "" && this.password2 != "") {
      this.http.post<UserJWT[]>(`${this.endpoint}/login`, {email, password})
        .subscribe((res: any) => {
  
          console.log(res);
  
          if(res[0].jwt != undefined){
  
            console.log("signing in")
  
            localStorage.setItem('access_token', res[0].jwt);
            localStorage.setItem('user_id', res[0].userUUID);
            
  
            this.router.navigate(['user-profile/' + res[0].userUUID]);
            // this.getUserProfile().subscribe((res) => {
            // this.signInResponse = res[0];
            // this.router.navigate(['user-profile/' + res[0].userUUID]);
            // });
          }
          else{
            console.log(res[0].message);
            this.toastr.success("Username or password was incorrect");
            this.showIncorrectUsernamePassword = 1;
          }
  
        });
    }else{
  
      console.log('error caught');
      this.toastr.success("An error occured");
      this.showErrorOccured = 1;
      
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.signinForm.controls; }

}




export interface Topic {
  topicName: string;
  topicDescription: string;
}

import { TestBed } from '@angular/core/testing';

import { CourseIdService } from './course-id.service';

describe('CourseIdService', () => {
  let service: CourseIdService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CourseIdService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

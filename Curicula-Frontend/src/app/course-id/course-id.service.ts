import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Videos } from '../videos/videos';
import { Courses } from '../courses/courses';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CourseIdService {
  url = 'https://curicular-api.herokuapp.com/courses?courseID=';
  videoURL = 'https://curicular-api.herokuapp.com/videos?courseID=';
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getCourse(courseID: number): Observable<any> {

    return this.http.get<Courses[]>(this.url + courseID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  getVideos(courseID: number): Observable<any> {

    return this.http.get<Videos[]>(this.videoURL + courseID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code:' + error.status + '\nMessage:' + error.message;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

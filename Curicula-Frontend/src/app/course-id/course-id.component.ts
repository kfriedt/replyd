import { CourseIdService } from './course-id.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { Courses } from '../courses/courses';
import { Videos } from '../videos/videos';


@Component({
  selector: 'app-course-id',
  templateUrl: './course-id.component.html',
  styleUrls: ['./course-id.component.scss']
})
export class CourseIdComponent implements OnInit, OnDestroy {


  courseID: number;

  course: Courses[] = [];
  videosArray: Videos[] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(private actRoute: ActivatedRoute, private courseIDService: CourseIdService) {
    this.courseID = this.actRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.populateCourses();
    this.populateVideos();
  }

  populateCourses() {
    this.courseIDService.getCourse(this.courseID).subscribe(dataRecv => {

      this.course = dataRecv;
      console.log(this.course);
    });
  }

  populateVideos() {
    this.courseIDService.getVideos(this.courseID).subscribe(dataRecv => {
      console.log(dataRecv);
      this.videosArray = dataRecv;
    });
  }

  getThumbnailLink(url: string) {
    url = url.replace('https://www.youtube.com/watch?v=','https://img.youtube.com/vi/');
    return url + '/0.jpg';
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

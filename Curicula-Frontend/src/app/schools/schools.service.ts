import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { School } from './schools';
@Injectable({
  providedIn: 'root'
})
export class SchoolsService {


  schoolsUrl = 'https://curicular-api.herokuapp.com/schools';
  constructor(private http: HttpClient) { }

  httpOptions = { headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };

  //method to get an observable object

  getSchools(): Observable<any> {
    console.log('Accessed school service');

    return this.http.get<School[]>(this.schoolsUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }
  //handle errors
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

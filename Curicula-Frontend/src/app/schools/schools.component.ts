import { SchoolsService } from './schools.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { School } from './schools';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-schools',
  templateUrl: './schools.component.html',
  styleUrls: ['./schools.component.scss']
})
export class SchoolsComponent implements OnInit, OnDestroy {

  schoolArray: School [] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private schoolService: SchoolsService) { }

  ngOnInit() {
    this.populateSchoolArray();
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  populateSchoolArray()
  {
    this.schoolService.getSchools().subscribe(dataRecv => {
      console.log(dataRecv);

      this.schoolArray = dataRecv;
      console.log()
    
    });
  }
}


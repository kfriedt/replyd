export interface VideoPage {
    videoID: string;
    videoLink: string;
    description: string;
    datePosted: string;
    relatedCourseID: number;
    verifiedFlag: number;
    posterID: string;
}
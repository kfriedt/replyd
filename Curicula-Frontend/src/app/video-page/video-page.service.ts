import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { VideoPage } from './video-page';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class VideoPageService {
  url = 'https://curicular-api.herokuapp.com/videos?videoID=';

  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getVideo(vidID: string): Observable<any> {
    console.log('RIGHT BELOW HERE');

    return this.http.get<VideoPage[]>(this.url + vidID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}

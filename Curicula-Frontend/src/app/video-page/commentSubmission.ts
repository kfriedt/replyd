export interface CommentSubmission {
    username: string,
    mode: string,
    comment_body: string,
    comment_level: number,
    target_id: string
}
export interface Comment {
    comment_id: string;
    author_user_id: number;
    author_username: string;
    body: string;
    time_stamp: string;
    //verifiedFlag: boolean;
    vote_count: number;
    child_count: number;
    //edited: boolean;
    //user_vote: number;
    children: Comment[];
}
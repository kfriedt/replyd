import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Comment } from './comment'
import { CommentSubmission } from './commentSubmission'
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { AuthService } from './../_shared/auth.service';
import { User } from '../_shared/user';

@Injectable({
  providedIn: 'root'
})
export class CommentService  {

  parenturl = 'https://curicular-api.herokuapp.com/comments/parent/';
  childurl = 'https://curicular-api.herokuapp.com/comments/child/';
  submiturl = 'https://curicular-api.herokuapp.com/comments/';
  typedCurrentUser: User;
  constructor(private http: HttpClient, public authService: AuthService,  ) {
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };


  getTopLevelComments(videoID: string): Observable<any> {
    console.log('Getting Parent comments');
    return this.http.get<Comment[]>(this.parenturl + "?videoID=" + videoID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  getChildComments(commentID: string): Observable<any> {
    console.log('Getting Child comments for comment  ' + commentID);

    return this.http.get<Comment[]>(this.childurl + "?commentID=" + commentID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }


  submitComment(parentID: string, body: string, commentLevel: number,user:string) {
    //console.log(this.typedCurrentUser);
    
    this.httpOptions.headers = this.httpOptions.headers.set('auth-token', this.authService.getToken());
    console.log(this.authService.getToken());
    //console.log(this.typedCurrentUser.userName);
    console.log(user);
    const sub: CommentSubmission = {
      username: user,
      mode: "create",
      comment_body: body,
      comment_level: commentLevel,
      target_id: parentID
    };

    console.log(sub);

    var ret = this.http.post<CommentSubmission[]>(this.submiturl, sub, this.httpOptions).pipe(retry(0), catchError(this.errorHandl));
    console.log("after sending http post");
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    console.log(ret);
    return ret;

  }


  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VideoPageService } from './video-page.service';
import { VideoPage } from './video-page';
import { Comment } from './comment'
import { CommentService } from './comment.service'
import { Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthService } from '../_shared/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { User } from '../_shared/user';

@Component({
  selector: 'app-video-page',
  templateUrl: './video-page.component.html',
  styleUrls: ['./video-page.component.scss']
})
export class VideoPageComponent implements OnInit {

  submitCommentForm: FormGroup;
  videoID: string;

  typedCurrentUser: User;
  videoArray: VideoPage[] = [];
  videoURL: string;
  commentArray: Comment[] = [];
  // One reply box open at a time, this helps to keep track of what is hidden or not. if empty then all closed
  openedCommentBoxID: string;

  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private actRoute: ActivatedRoute, private videoPageService: VideoPageService, private commentService: CommentService, private sanitizer: DomSanitizer, public authService: AuthService, public fb: FormBuilder) {
    this.videoID = this.actRoute.snapshot.params.id;
    this.openedCommentBoxID = "";
    this.submitCommentForm = this.fb.group({
      body: ['']

    });
  }

  ngOnInit(): void {
    this.populateVideoPageArray();
    this.getComments();
    this.authService.getUserProfile().subscribe(res => {
      this.typedCurrentUser = res[0];
      console.log("The current user is " + JSON.stringify(this.typedCurrentUser))
    });
    //this.makeURLUsable();
  }

  populateVideoPageArray() {
    this.videoPageService.getVideo(this.videoID).subscribe(dataRecv => {
      console.log(dataRecv);
      this.videoArray = dataRecv;
      console.log(this.videoArray[0].description);
      console.log();
      this.makeURLUsable();
    });
  }

  makeURLUsable() {
    this.videoURL = this.videoArray[0].videoLink;
    console.log(this.videoURL);
    this.videoURL = this.videoURL.replace("watch?v=", "embed/");
    console.log(this.videoURL);
    // this.videoURL = this.sanitizer.bypassSecurityTrustUrl(this.videoURL);
    // console.log(this.videoURL);

  }

  getComments() {
    this.commentService.getTopLevelComments(this.videoID).subscribe(dataRecv => {
      console.log(dataRecv);
      this.commentArray = dataRecv;
      console.log(this.commentArray[0].body);
      console.log();
    });

  }

  getChildComments(parentID: string) {
    this.commentArray.forEach(element => {
      if (element.comment_id == parentID) {
        this.commentService.getChildComments(parentID).subscribe(dataRecv => {
          console.log(dataRecv);
          element.children = dataRecv;
          console.log(this.commentArray[0].body);
          console.log();
        });
      }
    });
  }

  setOpenCommentBox(parentID: string) {
    this.openedCommentBoxID = parentID;
  }

  closeCommentBox() {
    this.openedCommentBoxID = "";
    this.submitCommentForm.reset();
  }

  onSubmit(parentID: string, commentLevel:number) {
    console.log(parentID);
    console.log(this.submitCommentForm.value.body);
    if(this.submitCommentForm.value.body===''){
      console.log("Empty comment, no submission");
      return;
    }
    this.commentService.submitComment(parentID,this.submitCommentForm.value.body,commentLevel,this.typedCurrentUser.userName).subscribe();

    if(commentLevel==1){
      this.getChildComments(parentID)
    }else if (commentLevel==0){
      this.getComments();
    }
    this.submitCommentForm.reset();
    //console.log("submit not yet implemented");
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

}

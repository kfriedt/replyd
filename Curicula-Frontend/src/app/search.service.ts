import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  searchBarObs$: Observable<any>;
  searchData: string;
  private searchBarSub = new Subject<any>();


    constructor() {
        this.searchBarObs$ = this.searchBarSub.asObservable();
    }

    setSearchData(data) {
      this.searchData = data;
    }
    broadcastSearchData() {
      this.searchBarSub.next(this.searchData);
    }
    getSearchData(data) {
        console.log(data); // I have data! Let's return it so subscribers can use it!
        // we can do stuff with data if we want
        this.searchBarSub.next(data);
    }
}

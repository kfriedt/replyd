import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoPageService {

  constructor() { }
}

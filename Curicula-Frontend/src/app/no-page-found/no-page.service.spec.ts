import { TestBed } from '@angular/core/testing';

import { NoPageService } from './no-page.service';

describe('NoPageService', () => {
  let service: NoPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NoPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

export interface Schools {
  schoolName: string;
  schoolID: string;
}

export interface Majors {
  programName: string;
  programID: string;
}

export interface Minors {
  programName: string;
  programID: string;
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Schools, Majors, Minors } from './register';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  schoolUrl = 'https://cors-anywhere.herokuapp.com/https://curicular-api.herokuapp.com/schools';
  programUrl = 'https://cors-anywhere.herokuapp.com/https://curicular-api.herokuapp.com/programs';
  // TODO may need another endpoint to get the minors for now we will just do it like this.
  constructor(private http: HttpClient) { }

  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };

  getSchools(): Observable<any> {
    return this.http.get<Schools[]>(this.schoolUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
  }

  getMajors(): Observable<any> {
    return this.http.get<Majors[]>(this.programUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
  }

  getMinors(): Observable<any> {
    return this.http.get<Minors[]>(this.programUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

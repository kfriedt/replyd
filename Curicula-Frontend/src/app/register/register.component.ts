import { RegisterService } from './register.service';
import { Component, OnInit, OnDestroy, Input, NgModule} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { AuthService } from '../_shared/auth.service';
import { Router } from '@angular/router';
import { Schools, Majors, Minors } from './register';

// New
import { SearchBarService } from '../search-bar/search-bar.service';
import { School } from '../schools/schools';
import { BrowserModule } from '@angular/platform-browser';


import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  // added
  @Input() term: string;

  showErrorOccured: number = 0;

  signupForm: FormGroup;
  submitted = false;

  // TOOD look into using this and populating it so that it is not O(n)
  // schoolArray: {[key: string]: string}; // school name: school id
  // majorArray: {[key: string]: string}; // Engineering
  // minorArray: {[key: string]: string};  // software electrical etc

  // Data gets stored in the ObsAr first we will then try to populate the key value pairs.
  schoolsObAr: Schools[] = [];
  majorsObAr: Majors[] = [];
  minorsObAr: Minors[] = [];

  // Testing async pipe
  schoolsObs: Observable<Schools>;

  // Testing predictive search
  schoolArr: School [] = [];

  // Destroy subscriptions after the page closes so that there are no memory leaks.
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    public fb: FormBuilder,
    public authService: AuthService,
    public router: Router,
    private registerService: RegisterService,

    // testing
    private searchService: SearchBarService
  ) {
    this.signupForm = this.fb.group({
      userName: [''],
      firstName: [''],
      lastName: [''],
      email: [''],
      schoolID: [''],
      majorProgramID: [''],
      minorProgramID: [''],
      password: ['']
    });
  }

  ngOnInit(): void {
    this.populateSchoolsObAr();
    this.populateMajorsObAr();
    this.populateMinorsObAr();

    // Testing async pipe
    this.schoolsObs = this.registerService.getSchools();

    // testing school predictive search
    this.searchService.getSchools().subscribe(dataRecv => {
      this.schoolArr = dataRecv;
    });

  }

  // convenience getter for easy access to form fields
  get f() { return this.signupForm.controls; }

  populateSchoolsObAr() {
    this.registerService.getSchools().subscribe(dataRecv => {
      console.log('Schools');
      console.log(dataRecv);
      this.schoolsObAr = dataRecv;
    });
  }
  
  populateMajorsObAr() {
    this.registerService.getMajors().subscribe(dataRecv => {
      console.log('Majors');
      console.log(dataRecv);
      this.majorsObAr = dataRecv;
    });
  }

  populateMinorsObAr() {
    this.registerService.getMinors().subscribe(dataRecv => {
      console.log('Minors');
      console.log(dataRecv);
      this.minorsObAr = dataRecv;
    });
  }


  // TODO check password fields match
  // TODO check that the userName is not taken
  // This returns the User-UUID in the header?
  registerUser() {

    this.submitted = true;

    this.processSchoolID(this.signupForm.value.schoolID);
    this.processMajorID(this.signupForm.value.majorProgramID);
    this.processMinorID(this.signupForm.value.minorProgramID);

    // console.log(this.signupForm.value);
    this.authService.signUp(this.signupForm.value).subscribe((res) => {

      // console.log(res[0].userUUID);
      if (res[0].userUUID) {
        this.signupForm.reset();
        this.router.navigate(['login']);
      } else {
        console.log('did not route to login because we did not get the right error');
        this.showErrorOccured = 1;
      }
    });
    
  }
  // TODO make it so a school id has to be selected
  processSchoolID(schoolName: string) {
    for (const schl of this.schoolsObAr) {
      if (schl.schoolName === schoolName) {
        this.signupForm.value.schoolID = schl.schoolID;
      }
    }
  }
  // TODO make it so a major id has to be selected
  processMajorID(major: string) {
    for (const maj of this.majorsObAr) {
      if (maj.programName === major) {
        this.signupForm.value.majorProgramID = maj.programID;
      }
    }
  }
  // TODO set it up so that the minor default value is 0
  processMinorID(minor: string) {
    for (const min of this.minorsObAr) {
      if (min.programName === minor) {
        this.signupForm.value.minorProgramID = min.programID;
      }
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
}

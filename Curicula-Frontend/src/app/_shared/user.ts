export interface User {
  userUUID: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  school?: string;
  major?: string;
  minor?: string;
}

import { Injectable } from '@angular/core';
import { UserReg } from './user-reg';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { UserJWT } from './user-jwt';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  endpoint: string = 'https://curicular-api.herokuapp.com/user';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  // we will be using the headers instead of these.

  signInResponse: User;

  constructor(private http: HttpClient, public router: Router, private toastr: ToastrService) { }

  // Sign-up  TODO  talk to Colton about what we need to send. If this userReg object will work
  signUp(userReg: UserReg): Observable<any> {
    let api = `${this.endpoint}/register`;
    return this.http.post(api, userReg)
      .pipe(
        catchError(this.handleError)
      );
  }

  // Sign-in  TODO this assumes that we are getting the jwtToken in 'token', and the user id in '_id'
  // We are actually getting userID: and jwt:
  // TOOD can we do this so the headers get updated? with auth and uuid?
  // TODO Set up so that it retry's
  // signIn(email: string, password: string) {

	// if (email != "" && password != "") {
	// 	this.http.post<UserJWT[]>(`${this.endpoint}/login`, {email, password})
  //     .subscribe((res: any) => {

  //       console.log(res);

  //       if(res[0].jwt != undefined){

  //         console.log("signing in")

  //         localStorage.setItem('access_token', res[0].jwt);
  //         localStorage.setItem('user_id', res[0].userUUID);
          
  //         return 1;


	// 		    // this.getUserProfile().subscribe((res) => {
  //         // this.signInResponse = res[0];
	// 			  // this.router.navigate(['user-profile/' + res[0].userUUID]);
	// 	    	// });
  //       }
  //       else{
  //         console.log(res[0].message);

  //         return 2;
  //       }

  //     });
	// }else{

  //   console.log('error caught');
  //   return 3;

		
	// }


  // }

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    let authToken = localStorage.getItem('access_token');
    return (authToken !== null) ? true : false;
  }

  doLogout() { // Sent the signInResponse because post needed another value.
    this.http.post<UserJWT[]>(`${this.endpoint}/logout`, this.signInResponse).subscribe();

    let removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['login']);
     }
  }

    // User profile  TODO talk to Colton about this route for the user profile
    getUserProfile(): Observable<any> {
      let api = `${this.endpoint}/user-profile`;
      //The below did not work
      // this.headers.append('auth-token', localStorage.getItem('access_token'));
      // this.headers.append('user-id', localStorage.getItem('user_id'));
      this.headers = this.headers.set('auth-token', localStorage.getItem('access_token'));
      this.headers = this.headers.set('user-id', localStorage.getItem('user_id'));
      return this.http.get(api, { headers: this.headers }).pipe(
        map((res: Response) => {
          return res || {};
        }),
        catchError(this.handleError)
      );
    }

    // Error
    handleError(error: HttpErrorResponse) {
      let msg = '';
      if (error.error instanceof ErrorEvent) {
        // client-side error
        msg = error.error.message;
      } else {
        // server-side error
        msg = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      return throwError(msg);
    }
}

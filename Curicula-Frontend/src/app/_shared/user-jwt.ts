export interface UserJWT { // Return type after login
  userUUID: string;
  jwt: string;
}

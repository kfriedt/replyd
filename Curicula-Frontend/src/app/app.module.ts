import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthconfigInterceptor } from './_shared/authconfig.interceptor';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SchoolsComponent } from './schools/schools.component';
import { VideosComponent } from './videos/videos.component';
import { ProgramsComponent } from './programs/programs.component';
import { TopicsComponent } from './topics/topics.component';
import { NoPageFoundComponent } from './no-page-found/no-page-found.component';
import { TopicIdComponent } from './topic-id/topic-id.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseIdComponent } from './course-id/course-id.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ProgramPageComponent } from './program-page/program-page.component';
import {HomeService} from './home/home.service';
import { VideoPageComponent } from './video-page/video-page.component';

import { SanitizePipe } from './sanitize.pipe';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NewVideoComponent } from './new-video/new-video.component';

import { ToastrModule } from 'ngx-toastr';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegisterComponent,
    LoginComponent,
    SchoolsComponent,
    VideosComponent,
    ProgramsComponent,
    TopicsComponent,
    NoPageFoundComponent,
    TopicIdComponent,
    CoursesComponent,
    CourseIdComponent,
    ProgramPageComponent,
    VideoPageComponent,
    SanitizePipe,
    UserProfileComponent,
    SearchBarComponent,
    NewVideoComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    HttpClientModule,
    Ng2SearchPipeModule,
    MatDialogModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    ToastrModule.forRoot({
      timeOut: 1000,
      positionClass: 'toast-top-right'
    })
  ],
  providers: [
    HomeService,
    {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthconfigInterceptor,
    multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

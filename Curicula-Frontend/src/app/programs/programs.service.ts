import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Program } from './programs';
@Injectable({
  providedIn: 'root'
})
export class ProgramsService {

  programsUrl = 'https://cors-anywhere.herokuapp.com/https://curicular-api.herokuapp.com/programs';

  constructor(private http: HttpClient) { }

  httpOptions = { headers: new HttpHeaders ({
    'Content-Type': 'application/json'
  })};


  getPrograms(): Observable<any>{
    console.log('Accessed school service');

    return this.http.get<Program[]>(this.programsUrl, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
  }
  
  //handle errors
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
      
    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
  
}
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProgramsService } from './programs.service';
import { Program } from './programs';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.component.html',
  styleUrls: ['./programs.component.scss']
})
export class ProgramsComponent implements OnInit, OnDestroy {

  programArray: Program [] = [];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private programService: ProgramsService) { }

  ngOnInit(): void {
    this.populateProgramArray();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }
  populateProgramArray()
  {
    this.programService.getPrograms().subscribe(dataRecv => {
      console.log(dataRecv);

      this.programArray = dataRecv;
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Program } from './program';
import { throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { School } from '../schools/schools';

@Injectable({
  providedIn: 'root'
})
export class ProgramPageService {

  programsUrl = 'https://curicular-api.herokuapp.com/programs';
  schoolsUrl = 'https://curicular-api.herokuapp.com/schools';
  constructor(private http: HttpClient) { }

  schoolName: string;
  facultyName: string;
  httpOptions = { headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };

  setNames(sch: string, fac: string) {
    this.schoolName = sch;
    this.facultyName = fac;
  }

  getSchoolName() {
    return this.schoolName;
  }

  getFacultyName() {
    return this.facultyName;
  }

  getSchools(schoolID: string) {
  return this.http.get<School[]>(this.schoolsUrl + '?schoolID=' + schoolID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
}

  getPrograms(schoolID: string) {
    // tslint:disable-next-line: max-line-length
    return this.http.get<Program[]>(this.programsUrl + '?schoolID=' + schoolID, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));
  }
  // handle errors
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}

export interface Program {
    programID: string;
    programName: string;
};

export interface Faculty {
  facultyName: string;
  facultyID: string;
}

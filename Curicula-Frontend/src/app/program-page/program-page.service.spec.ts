import { TestBed } from '@angular/core/testing';

import { ProgramPageService } from './program-page.service';

describe('ProgramPageService', () => {
  let service: ProgramPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProgramPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { School } from '../schools/schools';
import { Program } from './program';
import { ProgramPageService } from './program-page.service';

@Component({
  selector: 'app-program-page',
  templateUrl: './program-page.component.html',
  styleUrls: ['./program-page.component.scss']
})
export class ProgramPageComponent implements OnInit {
  schoolID: string;
  programID: string;


  programArray: Program [] = [];
  facultyArray = [
    { facultyName: 'Engineering',
      facultyID: '1'},
      { facultyName: 'Computer Science',
      facultyID: '2'},
      { facultyName: 'Science',
      facultyID: '3'}
    ];
  schoolArray: School[] = [];

  constructor(private actRoute: ActivatedRoute, private programService: ProgramPageService) {
      this.schoolID = this.actRoute.snapshot.params.id;

    }
    ngOnInit(): void {
      this.populateSchoolArray();
      this.populateProgramArray();
    }

    populateSchoolArray() {
      this.programService.getSchools(this.schoolID).subscribe(dataRecv => {
        console.log(dataRecv);
        this.schoolArray = dataRecv;
        console.log(this.schoolArray[0].schoolName);
        this.programService.setNames(dataRecv[0].schoolName, 'Engineering');
      });
    }
    setProgramID(id: string) {
      this.programID = id;
      console.log(this.programID);

    }
    populateProgramArray() {
      this.programService.getPrograms(this.schoolID).subscribe(dataRecv => {
        console.log(dataRecv);

        this.programArray = dataRecv;
        console.log(this.programArray[0].programID);



      });
    }
  }

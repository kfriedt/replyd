import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NewVideo } from './new-video';
import { Courses } from '../courses/courses';
import { CoursesService } from '../courses/courses.service';

@Component({
  selector: 'app-new-video',
  templateUrl: './new-video.component.html',
  styleUrls: ['./new-video.component.scss']
})
export class NewVideoComponent implements OnInit {

  coursesArr: Courses[] = [];

  constructor(public dialogRef: MatDialogRef<NewVideoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NewVideo,private coursesService:CoursesService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit(): void {
    this.coursesService.getCourses().subscribe(dataRecv => {
      console.log(dataRecv);
      this.coursesArr = dataRecv;
   });
  }

}

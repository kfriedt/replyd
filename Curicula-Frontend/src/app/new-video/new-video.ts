export interface NewVideo {
    videoLink: string;
    videoDescription: string;
    relatedCourseID: string;
  }
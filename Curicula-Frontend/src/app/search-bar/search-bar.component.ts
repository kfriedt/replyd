import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Subject } from 'rxjs';
import { SearchBarService } from './search-bar.service';
import { School } from '../schools/schools';
import { Program } from '../programs/programs';
import { Videos } from '../videos/videos';
import { Courses } from '../courses/courses';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { SearchService } from '../search.service';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit , OnDestroy{


  @Input() searchBarTerm: string;


  schoolArr: School [] = [];
  programArr: Program [] = [];
  videoArr: Videos [] = [];
  coursesArr: Courses[] = [];

  destroy$: Subject<boolean> = new Subject<boolean>();

  mySubscription: any; // USED TO REFRESH THE PAGE WHEN YOU CLICK SEARCH


  constructor(private searchService: SearchBarService, private router: Router, private searchBarService: SearchService) {
    console.log('in constructor');
    this.searchBarService.searchBarObs$.subscribe((data) => {
      this.searchBarTerm = data;
      console.log(this.searchBarTerm);
      // console.log(data);
      // console.log(this.searchResultStr);
    });

    // Explains what the next 11 lines of code are for.
    // https://medium.com/@rakshitshah/refresh-angular-component-without-navigation-148a87c2de3f
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };

    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
   }

  ngOnInit(): void {
    console.log('in init');
    this.populateContentArray();
    this.searchBarService.broadcastSearchData();
  }


  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }

  populateContentArray() {
    this.searchService.getSchools().subscribe(dataRecv => {
      this.schoolArr = dataRecv;
    });

    this.searchService.getPrograms().subscribe(dataRecv => {
      this.programArr = dataRecv;
    });

    this.searchService.getVideos().subscribe(dataRecv => {
      this.videoArr = dataRecv;
    });

    this.searchService.getCourses().subscribe(dataRecv => {
      this.coursesArr = dataRecv;
    });

  }
}

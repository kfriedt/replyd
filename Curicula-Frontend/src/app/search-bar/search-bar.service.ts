import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


import { School } from '../schools/schools';
import { Program } from '../programs/programs';
import { Courses } from '../courses/courses';
import { Videos } from '../videos/videos';
import { Faculty } from '../_shared/faculty';

import { SchoolsService } from '../schools/schools.service';
import { ProgramsService } from '../programs/programs.service';
import { VideosService } from '../videos/videos.service';
import { CoursesService } from '../courses/courses.service';
import { FacultyService } from '../_shared/faculty.service';


@Injectable({
  providedIn: 'root'
})
export class SearchBarService {

  // tslint:disable-next-line: max-line-length
  constructor(private http: HttpClient, private facultyService: FacultyService, private schoolService: SchoolsService, private programService: ProgramsService, private videosService: VideosService, private coursesService: CoursesService) { }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  // method to get an observable object


  getSchools(): Observable<any> {
    // console.log("Getting Schools");
    return this.http.get<School[]>(this.schoolService.schoolsUrl).pipe(retry(3),catchError(this.errorHandl));
  }

  getPrograms(): Observable<any> {
    // console.log("Getting Programs");
    return this.http.get<Program[]>(this.programService.programsUrl).pipe(retry(3),catchError(this.errorHandl));
  }

  getCourses(): Observable<any> {
    // console.log("Getting Courses");
    return this.http.get<Courses[]>(this.coursesService.url).pipe(retry(3),catchError(this.errorHandl));
  }

  getVideos(): Observable<any> {
    // console.log("Getting Videos");
    return this.http.get<Videos[]>(this.videosService.url).pipe(retry(3),catchError(this.errorHandl));
  }



  //handle errors
  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

}

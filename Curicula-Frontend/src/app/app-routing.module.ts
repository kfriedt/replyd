
import { VideoPageComponent } from './video-page/video-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProgramsComponent } from './programs/programs.component';
import { SchoolsComponent } from './schools/schools.component';
import { TopicsComponent } from './topics/topics.component';
import { VideosComponent } from './videos/videos.component';
import { TopicIdComponent } from './topic-id/topic-id.component';

import { NoPageFoundComponent } from './no-page-found/no-page-found.component';
import { CoursesComponent } from './courses/courses.component';
import { CourseIdComponent } from './course-id/course-id.component';
import { ProgramPageComponent } from './program-page/program-page.component';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { AuthGuard } from './_shared/auth.guard';

import { SearchBarComponent } from './search-bar/search-bar.component';



const routes: Routes = [

  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'programs', component: ProgramsComponent },
  { path: 'program-page/:id', component: ProgramPageComponent},
  { path: 'courses', component: CoursesComponent },
  { path: 'course-id/:id', component: CourseIdComponent},
  { path: 'register', component: RegisterComponent },
  { path: 'schools', component: SchoolsComponent },
  { path: 'topics', component: TopicsComponent },
  { path: 'search-bar', component: SearchBarComponent},
  { path: 'videos', component: VideosComponent },
  { path: 'video-page/:id', component: VideoPageComponent },
  { path: 'topic-id/:id', component: TopicIdComponent },
  { path: 'user-profile/:id', component: UserProfileComponent, canActivate: [AuthGuard] },
  { path: '**', component: NoPageFoundComponent }
];
// https://stackoverflow.com/questions/47813927/how-to-refresh-a-component-in-angular  RELOAD REFERENCE
@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})], // THIS DIDN'T WORK reload is so the search function works
  exports: [RouterModule]
})
export class AppRoutingModule { }

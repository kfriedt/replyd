export interface Courses {
    courseName: string;
    courseID: string;
    courseLevel: string;
    schoolID: string;
    programID: string;
}

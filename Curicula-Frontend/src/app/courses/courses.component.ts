import { FacultyService } from './../_shared/faculty.service';
import { Faculty } from './../program-page/program';

import { debounceTime } from 'rxjs/operators';

import { Subject } from 'rxjs';
import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';


import { Courses } from './courses';
import { CoursesService } from './courses.service';

import { SearchBarService } from '../search-bar/search-bar.service';
import { School } from '../schools/schools';
import { Program } from '../programs/programs';
import { ProgramPageService } from '../program-page/program-page.service';
import { DataSource } from '@angular/cdk/collections';


@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.scss']
})
export class CoursesComponent implements OnInit, OnDestroy {

  @Input() schoolFilter: string;
  @Input() facultyFilter: string;
  @Input() majorFilter: string;
  @Input() yearFilter: string;

  schoolArr: School [] = [];
  levelsArr = ['2', '3', '4' , '5'];  // TODO fix levels some how
  coursesArr: Courses[] = [];
  programArr: Program [] = [];
  facutlyArr: Faculty [] = [];


  destroy$: Subject<boolean> = new Subject<boolean>();

  public schoolFilterSub: Subject<string> = new Subject();
  @Output() setSchool: EventEmitter<string> = new EventEmitter();

  // tslint:disable-next-line: max-line-length
  constructor(private coursesService: CoursesService, private facService: FacultyService, private searchService: SearchBarService, private progPageService: ProgramPageService ) {

    this.setSchoolDataList(); // TODO don't need this
   }

  ngOnInit(): void {
    this.populateCoursesArray();
    this.populateSchoolsArray();
    this.populateProgramsArray();
    this.populateFacultyArray();

    this.schoolFilter = this.progPageService.getSchoolName();
    this.facultyFilter = this.progPageService.getFacultyName();
    console.log(this.schoolFilter);
    console.log(this.facultyFilter);
    if (this.schoolFilter){
      (document.getElementById('schoolFilterInput') as HTMLInputElement).value = this.schoolFilter;
    }
    if (this.facultyFilter) {
      (document.getElementById('facultyFilterInput') as HTMLInputElement).value = this.facultyFilter;
    }



  }

  private setSchoolDataList() { // TODO don't need this
    this.schoolFilterSub.pipe(
      debounceTime(500)).subscribe((value: string) => {
        this.setSchool.emit(value);
      });
  }

  setYearFilter(year: string) {

    this.yearFilter = year;
  }
  setSchoolFilter(school: string) {
    this.setSchool.next(school);
    this.schoolFilter = school;
  }

  setFacultyFilter(fac: string ) {
    this.facultyFilter = fac;
  }

  setMajorFilter(maj: string) {
    this.majorFilter = maj;
  }

  populateFacultyArray() {
    this.facService.getFaculty().subscribe(dataRecv => {
      console.log(dataRecv);
      this.facutlyArr = dataRecv;
    });
  }
  populateCoursesArray() {
    this.coursesService.getCourses().subscribe(dataRecv => {
      console.log(dataRecv);
      this.coursesArr = dataRecv;


   });
  }
  populateSchoolsArray() {
    this.searchService.getSchools().subscribe(dataRecv => {
      this.schoolArr = dataRecv;
      console.log(dataRecv);
    });
  }

  populateProgramsArray() {
    this.searchService.getPrograms().subscribe(dataRecv => {
      this.programArr = dataRecv;
      console.log(dataRecv);
    });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }





}

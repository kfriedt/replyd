import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Courses } from './courses';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  url = 'https://curicular-api.herokuapp.com/courses';

  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
    'Content-Type': 'application/json'
    })
  };

  getCourses(): Observable<any>{
    console.log('RIGHT BELOW HERE');

    return this.http.get<Courses[]>(this.url, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 
@Component({
  selector: 'app-topic-id',
  templateUrl: './topic-id.component.html',
  styleUrls: ['./topic-id.component.scss']
})
export class TopicIdComponent implements OnInit {

  topic_id: string; //id number for the topic used to display page  

  constructor(private actRoute: ActivatedRoute) {
    this.topic_id = this.actRoute.snapshot.params.id;
   }

  ngOnInit(): void {
  }

}

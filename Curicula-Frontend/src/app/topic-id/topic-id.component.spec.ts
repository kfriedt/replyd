import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicIdComponent } from './topic-id.component';

describe('TopicIdComponent', () => {
  let component: TopicIdComponent;
  let fixture: ComponentFixture<TopicIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopicIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

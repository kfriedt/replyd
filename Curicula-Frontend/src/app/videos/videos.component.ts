import { Component, OnInit, OnDestroy, Input } from '@angular/core';

import { Subject } from 'rxjs';

import { Videos } from './videos';
import { VideosService } from './videos.service';
import { Program } from '../programs/programs';
import { Courses } from '../courses/courses';
import { School } from '../schools/schools';
import { Faculty } from '../_shared/faculty';
import { FacultyService } from '../_shared/faculty.service';
import { SearchBarService } from '../search-bar/search-bar.service';
import { MatDialog } from '@angular/material/dialog';
import { NewVideoComponent } from '../new-video/new-video.component';
import { NewVideo } from '../new-video/new-video';

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.scss']
})
export class VideosComponent implements OnInit, OnDestroy {


  @Input() schoolFilter: string;
  @Input() programFilter: string;
  @Input() courseFilter: string;
  @Input() yearFilter: string;

  schoolArr: School [] = [];

  coursesArr: Courses[] = [];
  programArr: Program [] = [];
  facultyArr: Faculty [] = [];          //= ['Engineering', 'Computer Science', 'Business'];
  videosArray: Videos[] = [];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(private videosService: VideosService, private facultyService: FacultyService, private searchBarService: SearchBarService,public dialog: MatDialog) {

   }

  ngOnInit(): void {
    this.populateVideosArray();
    this.populateCoursesArray();
    this.populateSchoolsArray();
    this.populateProgramsArray();
    this.populateFacultyArray();

  }


  populateFacultyArray() {
    this.facultyService.getFaculty().subscribe(dataRecv => {
      this.facultyArr = dataRecv;

    });
  }
  populateVideosArray() {
    this.videosService.getVideos().subscribe(dataRecv => {
      console.log(dataRecv);


      this.videosArray = dataRecv;
      console.log(this.videosArray[0].description);
      console.log();
   });
  }


  getThumbnailLink(url: string) {
    url = url.replace('https://www.youtube.com/watch?v=', 'https://img.youtube.com/vi/');
    return url + '/0.jpg';
  }

  setYearFilter(year: string) {
    this.yearFilter = year;
  }
  setSchoolFilter(school: string) {
    this.schoolFilter = school;
    console.log('school: ' + this.schoolFilter);
  }
  setProgramFilter(prog: string) {
    console.log('program value: ' + prog);

    this.programFilter = prog;
    console.log('program: ' + this.programFilter);
  }
  setCourseFilter(maj: string) {
    this.courseFilter = maj;
  }

  populateCoursesArray() {
    this.searchBarService.getCourses().subscribe(dataRecv => {
      console.log(dataRecv);
      this.coursesArr = dataRecv;
   });
  }

  populateSchoolsArray() {
    this.searchBarService.getSchools().subscribe(dataRecv => {
      this.schoolArr = dataRecv;
      console.log(dataRecv);
    });
  }

  populateProgramsArray() {
    this.searchBarService.getPrograms().subscribe(dataRecv => {
      this.programArr = dataRecv;
      console.log(dataRecv);
    });
  }

  openNewVideoDialog(): void {
    let newVid: NewVideo = {videoLink:" ", videoDescription:" ", relatedCourseID: " "};
    const dialogRef = this.dialog.open(NewVideoComponent, {
      width: '400px',
      data: newVid
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.submitNewVideo(newVid);
    });
  }
  submitNewVideo(newVid:NewVideo){
    console.log(newVid);
    this.videosService.submitVideo(newVid).subscribe();
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // Unsubscribe from the subject
    this.destroy$.unsubscribe();
  }


}

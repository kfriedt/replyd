import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Videos } from './videos';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { NewVideo } from '../new-video/new-video';
import { AuthService } from './../_shared/auth.service';
import { User } from '../_shared/user';
@Injectable({
  providedIn: 'root'
})
export class VideosService {

  typedCurrentUser: User[] = [];

  url = 'https://curicular-api.herokuapp.com/videos';
  posturl = 'https://curicular-api.herokuapp.com/video';
  constructor(private http: HttpClient, public authService: AuthService) {
    this.authService.getUserProfile().subscribe(res => {
      this.typedCurrentUser[0] = res[0];
    });
  }

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  getVideos(): Observable<any> {
    console.log('RIGHT BELOW HERE');

    return this.http.get<Videos[]>(this.url, this.httpOptions).pipe(retry(3), catchError(this.errorHandl));

  }

  submitVideo(newVid: NewVideo) {
    console.log(this.typedCurrentUser[0]);

    this.httpOptions.headers = this.httpOptions.headers.set('auth-token', this.authService.getToken());
    this.httpOptions.headers = this.httpOptions.headers.set('user-id', this.typedCurrentUser[0].userUUID);
    console.log(this.authService.getToken());
    console.log(this.typedCurrentUser[0].userName);


    console.log(newVid);

    var ret = this.http.post<NewVideo[]>(this.posturl, newVid, this.httpOptions).pipe(retry(0), catchError(this.errorHandl));
    console.log("after sending http post");
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    console.log(ret);
    return ret;

  }

  errorHandl(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;

    } else {
      errorMessage = 'Error Code: ${error.status}\nMessage: ${error.message}';
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }


}

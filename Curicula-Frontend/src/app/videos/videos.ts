export interface Videos {
    videoID: number;
    videoLink: string;
    description: string;
    datePosted: string;
    relatedCourseID: string;
    verifiedFlag: number;
    posterID: string;
  }
